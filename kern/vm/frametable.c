#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>
#include <frametable.h>

/* Place your frametable data-structures here 
 * You probably also want to write a frametable initialisation
 * function and call it from vm_bootstrap
 */

/* Judge alloc page before VM boostrap or not */
bool vm_boostrap_load;

static struct spinlock stealmem_lock = SPINLOCK_INITIALIZER;

/* Frametable shared with mutiple thread, to avoid racing condition add lock */
static struct spinlock frametable_lock = SPINLOCK_INITIALIZER;


/* Before VM Boostrap, alloc page */
paddr_t getppages(unsigned long npages)
{
        paddr_t addr;
        spinlock_acquire(&stealmem_lock);
        // Initial pages
        addr = ram_stealmem(npages);
        spinlock_release(&stealmem_lock);
        return addr;
}

/* After VM Boostrap, alloc page */
paddr_t alloc_page(unsigned long npages)
{
        /* Assume npage == 1 */
        (void)npages;

        paddr_t addr;
        spinlock_acquire(&frametable_lock);
        unsigned long page_start = 0;
        unsigned long i;

        // Finding first free page
        for (i = 0; i < pages_in_table; i++)
        {

                if (frametable[i].flag == 0)
                {
                        break;
                }
                page_start = i + 1;
        }

        // Can't find any free frame, out of memory
        if (i >= pages_in_table)
        {
                spinlock_release(&frametable_lock);
                return 0;
        }

        frametable[page_start].flag = 1;
        frametable[page_start].page_start = page_start;
        addr = frametable[page_start].pa;

        spinlock_release(&frametable_lock);
        return addr;
}

/* Note that this function returns a VIRTUAL address, not a physical 
 * address
 * WARNING: this function gets called very early, before
 * vm_bootstrap().  You may wish to modify main.c to call your
 * frame table initialisation function, or check to see if the
 * frame table has been initialised and call ram_stealmem() otherwise.
 */

/* Kernel-space alloc frame */
vaddr_t alloc_kpages(unsigned long npages)
{
        // Only can handle one page alloc
        if (npages > 1)
        {
                return 0;
        }

        paddr_t addr;
        if (vm_boostrap_load)
        {
                /* After VM Boostrap, use frametable to alloc */
                addr = alloc_page(npages);
        }
        else
        {
                /* Before VM Boostrap, use ram_stealmem */
                addr = getppages(npages);
        }

        if (addr == 0)
        {
                return 0;
        }
        /* Convert physical address to virtral address */
        return PADDR_TO_KVADDR(addr);
}

/* Kernel-space free frame */
void free_kpages(vaddr_t addr)
{
        int pop = 0;
        unsigned long i;

        spinlock_acquire(&frametable_lock);
        for (i = 0; i < pages_in_table; i++)
        {
                if (frametable[i].va == addr)
                {
                        pop = frametable[i].page_start;
                        break;
                }
        }

        while (frametable[i].page_start == pop)
        {
                frametable[i].flag = 0;
                i++;
        }

        spinlock_release(&frametable_lock);
}

/* User-space alloc frame */
paddr_t alloc_upages(int npages)
{
        // Only can handle one page alloc
        if (npages > 1)
        {
                return 0;
        }

        paddr_t addr;
        if (vm_boostrap_load)
        {
                addr = alloc_page(npages);
        }
        else
        {
                addr = getppages(npages);
        }

        return addr;
}

/* User-space free frame */
void free_upages(paddr_t addr)
{
        unsigned long i;
        spinlock_acquire(&frametable_lock);
        for (i = 0; i < pages_in_table; i++)
        {
                if (frametable[i].pa == addr)
                {
                        frametable[i].flag = 0;
                        break;
                }
        }
        spinlock_release(&frametable_lock);
}