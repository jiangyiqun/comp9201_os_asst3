#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>
#include <machine/tlb.h>
#include <frametable.h>
#include <current.h>
#include <proc.h>
#include <spl.h>

bool vm_boostrap_load = false;

void vm_bootstrap(void)
{
        int page_num, frametable_size;
        paddr_t lastaddr, firstaddr, freeaddr;

        /* Get RAM size */
        ram_getrange(&firstaddr, &lastaddr);

        /* Number of frames is the size of RAM divided by size of page */
        /* 4K per page */
        page_num = (lastaddr - firstaddr) / PAGE_SIZE;

        /* Address start after frametable */
        freeaddr = firstaddr + page_num * sizeof(struct page);
        freeaddr = ROUNDUP(freeaddr, PAGE_SIZE);

        /* Calculate frametable address */
        frametable = (struct page *)PADDR_TO_KVADDR(firstaddr);
        paddr_t frametable_addr = freeaddr - firstaddr;
        frametable_size = ROUNDUP(frametable_addr, PAGE_SIZE) / PAGE_SIZE;

        /* Global var pages_in_table */
        pages_in_table = page_num;

        /* Create frame table entries for all frames */
        for (int i = 0; i < page_num; i++)
        {
                if (i < frametable_size)
                {
                        frametable[i].flag = 1;
                }
                else
                {
                        frametable[i].flag = 0;
                }
                paddr_t temp = PAGE_SIZE * i + freeaddr;
                frametable[i].pa = temp;
                frametable[i].va = PADDR_TO_KVADDR(temp);
        }
        vm_boostrap_load = true;
}

/* Handles TLB miss by searching the pagetable. If entry
 * does not exist, creates a new page table entry.
 */
int vm_fault(int faulttype, vaddr_t faultaddress)
{
        vaddr_t stackbase, stacktop;
        paddr_t paddr = 0;
        int i;
        uint32_t ehi, elo;
        struct addrspace *as;
        int spl;

        faultaddress &= PAGE_FRAME;

        DEBUG(DB_VM, "dumbvm: fault: 0x%x\n", faultaddress);

        switch (faulttype)
        {
        case VM_FAULT_READONLY:
                /* We always create pages read-write, so we can't get this */
                panic("dumbvm: got VM_FAULT_READONLY\n");
        case VM_FAULT_READ:
        case VM_FAULT_WRITE:
                break;
        default:
                return EINVAL;
        }

        if (curproc == NULL)
        {
                return EFAULT;
        }

        // Get current addressspace for current thread
        as = proc_getas();
        if (as == NULL)
        {
                return EFAULT;
        }

        /* Assert that the address space has been set up properly. */
        KASSERT(as->heap_end != 0);
        KASSERT(as->heap_start != 0);
        KASSERT(as->pages != NULL);
        KASSERT(as->stack != NULL);
        KASSERT(as->heap != NULL);
        KASSERT(as->regions != NULL);
        KASSERT((as->pages->vaddr & PAGE_FRAME) == as->pages->vaddr);

        stackbase = USERSTACK - VM_STACKPAGES * PAGE_SIZE;
        stacktop = USERSTACK;

        struct page_table_entry *pte;

        /* Try to find address */
        if (faultaddress >= stackbase && faultaddress < stacktop)
        {
                pte = as->stack;
        }
        else
        {
                pte = as->pages;
        }

        while (pte != NULL)
        {
                if (faultaddress >= pte->vaddr && faultaddress < (pte->vaddr + PAGE_SIZE))
                {
                        paddr = (faultaddress - pte->vaddr) + pte->paddr;
                        break;
                }
                /* Check next page */
                pte = pte->next;
        }

        if (paddr == 0)
        {
                return EFAULT;
        }

        /* Make sure it's page-aligned */
        KASSERT((paddr & PAGE_FRAME) == paddr);

        /* Disable interrupts on this CPU while frobbing the TLB. */
        spl = splhigh();

        for (i = 0; i < NUM_TLB; i++)
        {
                tlb_read(&ehi, &elo, i);
                if (elo & TLBLO_VALID)
                {
                        continue;
                }
                ehi = faultaddress;
                elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
                DEBUG(DB_VM, "dumbvm: 0x%x -> 0x%x\n", faultaddress, paddr);
                tlb_write(ehi, elo, i);
                splx(spl);
                return 0;
        }

        ehi = faultaddress;
        elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
        DEBUG(DB_VM, "dumbvm: 0x%x -> 0x%x\n", faultaddress, paddr);
        tlb_random(ehi, elo);
        splx(spl);
        return 0;
}

/*
 *
 * SMP-specific functions.  Unused in our configuration.
 */

void vm_tlbshootdown(const struct tlbshootdown *ts)
{
        (void)ts;
        panic("vm tried to do tlb shootdown?!\n");
}