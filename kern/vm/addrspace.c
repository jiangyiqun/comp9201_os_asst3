/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *        The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <vm.h>
#include <proc.h>
#include <frametable.h>

/*
 * Note! If OPT_DUMBVM is set, as is the case until you start the VM
 * assignment, this file is not compiled or linked or in any way
 * used. The cheesy hack versions in dumbvm.c are used instead.
 *
 * UNSW: If you use ASST3 config as required, then this file forms
 * part of the VM subsystem.
 *
 */

struct addrspace *
as_create(void)
{
	struct addrspace *as;

	as = kmalloc(sizeof(struct addrspace));
	if (as == NULL)
	{
		return NULL;
	}

	as->pages = NULL;
	as->regions = NULL;
	as->stack = NULL;
	as->heap = NULL;
	as->heap_end = (vaddr_t)0;
	as->heap_start = (vaddr_t)0;

	return as;
}

int as_copy(struct addrspace *old, struct addrspace **ret)
{
	struct addrspace *newas;

	newas = as_create();
	if (newas == NULL)
	{
		return ENOMEM;
	}

	struct regionlist *itr, *newitr, *tmp;
	itr = old->regions;

	while (itr != NULL)
	{
		if (newas->regions == NULL)
		{
			newas->regions = (struct regionlist *)kmalloc(sizeof(struct regionlist));
			newas->regions->next = NULL;
			newitr = newas->regions;
		}
		else
		{
			for (tmp = newas->regions; tmp->next != NULL; tmp = tmp->next)
				;
			newitr = (struct regionlist *)kmalloc(sizeof(struct regionlist));
			tmp->next = newitr;
		}

		newitr->vbase = itr->vbase;
		newitr->pbase = itr->pbase;
		newitr->npages = itr->npages;
		newitr->permissions = itr->permissions;
		newitr->next = NULL;

		itr = itr->next;
		//newitr->last=itr->last;
	}

	// Now actually allocate new pages for these regions
	if (as_prepare_load(newas))
	{
		as_destroy(newas);
		return ENOMEM;
	}

	// Copy the data from old to new
	struct page_table_entry *iterate1 = old->pages;
	struct page_table_entry *iterate2 = newas->pages;

	while (iterate1 != NULL)
	{

		memmove((void *)PADDR_TO_KVADDR(iterate2->paddr),
				(const void *)PADDR_TO_KVADDR(iterate1->paddr), PAGE_SIZE);

		iterate1 = iterate1->next;
		iterate2 = iterate2->next;
	}

	(void)old;

	*ret = newas;
	return 0;
}

void as_destroy(struct addrspace *as)
{
	/*
         * Clean up as needed.
         */

	if (as != NULL)
	{
		struct regionlist *reglst = as->regions;
		struct regionlist *temp;
		while (reglst)
		{
			temp = reglst;
			reglst = reglst->next;
			kfree(temp);
		}

		struct page_table_entry *pte = as->pages;
		struct page_table_entry *pagetemp;
		while (pte != NULL)
		{

			pagetemp = pte;
			pte = pte->next;

			free_upages(pagetemp->paddr);

			kfree(pagetemp);
		}
	}
	kfree(as);
}

/* Flush by writing invalid data to the TLB. */
void as_tlb_invalid(struct addrspace *as)
{
	int i, spl;
	if (as == NULL)
	{
		/*
         * Kernel thread without an address space; leave the
         * prior address space in place.
         */
		return;
	}

	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();
	for (i = 0; i < NUM_TLB; i++)
	{
		tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
	}
	splx(spl);
}

// Copy from DumbVM
void as_activate(void)
{
	struct addrspace *as;
	as = proc_getas();
	as_tlb_invalid(as);
}

void as_deactivate(void)
{
	struct addrspace *as;
	as = proc_getas();
	as_tlb_invalid(as);
}

/*
 * Set up a segment at virtual address VADDR of size MEMSIZE. The
 * segment in memory extends from VADDR up to (but not including)
 * VADDR+MEMSIZE.
 *
 * The READABLE, WRITEABLE, and EXECUTABLE flags are set if read,
 * write, or execute permission should be set on the segment. At the
 * moment, these are ignored. When you write the VM system, you may
 * want to implement them.
 */
int as_define_region(struct addrspace *as, vaddr_t vaddr, size_t memsize,
					 int readable, int writeable, int executable)
{

	size_t npages;

	/* Align the region. First, the base */
	memsize += vaddr & ~(vaddr_t)PAGE_FRAME;
	vaddr &= PAGE_FRAME;

	/* Length */
	memsize = (memsize + PAGE_SIZE - 1) & PAGE_FRAME;
	npages = memsize / PAGE_SIZE;

	// kprintf("VADDR: %d\n", vaddr);
	// kprintf("PAGES: %d\n", npages);

	/* Set region chain */
	struct regionlist *end;
	if (as->regions != NULL)
	{
		end = as->regions->last;
	}

	if (as->regions == NULL)
	{
		as->regions = (struct regionlist *)kmalloc(sizeof(struct regionlist));
		as->regions->next = NULL;
		as->regions->last = as->regions;
		end = as->regions;
	}
	else
	{
		end = as->regions->last;
		end->next = (struct regionlist *)kmalloc(sizeof(struct regionlist));
		end = end->next;
		end->next = NULL;
		as->regions->last = end;
	}

	end->vbase = vaddr;
	end->npages = npages;
	end->pbase = 0;
	end->permissions = 7 & (readable | writeable | executable);

	return 0;
}

int as_prepare_load(struct addrspace *as)
{
	paddr_t paddr = 0;
	vaddr_t vaddr = 0;

	// Setting up page table
	struct regionlist *regionlst;
	struct page_table_entry *pages;
	regionlst = as->regions;
	while (regionlst != NULL)
	{
		vaddr = regionlst->vbase;
		for (size_t i = 0; i < regionlst->npages; i++)
		{
			/* Alloc page in region */
			if (as->pages == NULL)
			{
				as->pages = (struct page_table_entry *)kmalloc(sizeof(struct page_table_entry));
				as->pages->vaddr = vaddr;
				as->pages->permissions = regionlst->permissions;
				as->pages->next = NULL;
				paddr = alloc_upages(1);
				if (paddr == 0)
				{
					// Alloc error, not get page
					return ENOMEM;
				}

				as->pages->paddr = paddr;
			}
			else
			{
				for (pages = as->pages; pages->next != NULL; pages = pages->next)
					;
				pages->next = (struct page_table_entry *)kmalloc(sizeof(struct page_table_entry));
				pages->next->vaddr = vaddr;
				pages->next->permissions = regionlst->permissions;
				pages->next->next = NULL;
				paddr = alloc_upages(1);
				if (paddr == 0)
				{
					// Triple Huge Error
					return ENOMEM;
				}
				pages->next->paddr = paddr;
			}

			vaddr += PAGE_SIZE;
		}

		regionlst = regionlst->next;
	}

	vaddr_t stackvaddr = USERSTACK - VM_STACKPAGES * PAGE_SIZE;
	for (pages = as->pages; pages->next != NULL; pages = pages->next)
		;
	
	/* Stack pages  */
	for (int i = 0; i < VM_STACKPAGES; i++)
	{
		struct page_table_entry *stack = (struct page_table_entry *)kmalloc(sizeof(struct page_table_entry));
		pages->next = stack;
		if (i == 0)
		{
			as->stack = stack;
		}
		stack->vaddr = stackvaddr;
		stack->next = NULL;
		paddr = alloc_upages(1);
		if (paddr == 0)
		{
			return ENOMEM;
		}
		stack->paddr = paddr;

		stackvaddr = stackvaddr + PAGE_SIZE;
		pages = pages->next;
	}

	struct page_table_entry *heap_page = (struct page_table_entry *)kmalloc(sizeof(struct page_table_entry));
	pages->next = heap_page;
	heap_page->next = NULL;

	paddr = alloc_upages(1);
	if (paddr == 0)
	{
		return ENOMEM;
	}

	heap_page->paddr = paddr;
	heap_page->vaddr = vaddr;

	as->heap_start = as->heap_end = vaddr;
	as->heap = heap_page;

	KASSERT(as->heap_start != 0);
	KASSERT(as->heap_end != 0);
	return 0;
}

int as_complete_load(struct addrspace *as)
{
	struct regionlist *cur = as->regions;
	while (cur != NULL)
	{
		cur->permissions = cur->permissions;
		cur = cur->next;
	}
	as_tlb_invalid(as);
	return 0;
}

int as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{
	(void)as;

	/* Initial user-level stack pointer */
	*stackptr = USERSTACK;
	return 0;
}