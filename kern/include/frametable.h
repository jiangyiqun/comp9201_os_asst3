#ifndef _FRAMETABlE_H_
#define _FRAMETABLE_H_

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <addrspace.h>
#include <vm.h>

struct page {
    /* Virtual Address */
    vaddr_t va;
    /* Physical Address */
    paddr_t pa;
    /* offset */
    int page_start;
    /* To judge belong to frametable or not  */
    int flag;
};

unsigned long pages_in_table;
struct page* frametable;

// Kernel 
vaddr_t alloc_kpages(unsigned long npages);
void free_kpages(vaddr_t addr);

// Before VM boostrap alloc method
paddr_t getppages(unsigned long npages);

// After VM boostrap alloc method
paddr_t alloc_page(unsigned long npages);

// User 
paddr_t alloc_upages(int npages);
void free_upages(paddr_t addr);

#endif 